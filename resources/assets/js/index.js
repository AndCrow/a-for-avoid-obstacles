import Draw from "./draw.js"; //View
import Point from "./point.js";
import Astar from './astar';

export default class Controller{
    constructor(){
        this.cellsCount = 0;
        this.cells = [];

        this.changeCellsCount(10);
    }

    changeCellsCount(number) {
        this.cellsCount = number;

        //TODO: save
        this.cells = [];

        for (let i = 0; i < number; i++){
            for (let j = 0; j < number; j ++){
                if (!this.cells[i]){
                    this.cells[i] = [];
                }
                if (!this.cells[i][j]){
                    this.cells[i][j] = new Point(i, j);
                }
            }
        }

        this.draw = new Draw(document.getElementById('canvas'), this.cells);
    }

    clear(){
        this.changeCellsCount(10);
    }

    changeMarker(marker) {
        this.draw.changeMarker(marker);
    }

    findPath(){
        let alg = new Astar(this.draw.cells);

        if (alg.findPath()) {
            this.draw.setCells(alg.points);
        } else {
            this.draw.setCells(alg.points);
            alert('can\'t find a way!')
        }
    }
}

var ctrl = new Controller();

//grid input process
$("input#grid").on('change', (e) => {
    let cellsCount = parseInt($("input#grid").val());

    if (cellsCount > 1000){
        cellsCount = 1000;
    }

    if (cellsCount < 0){
        cellsCount = 0;
    }

    parseInt($("input#grid").val(cellsCount));
    ctrl.changeCellsCount(cellsCount);
});

//marker
$('.button-start').on('click', (e)=>{
    ctrl.changeMarker('start');
});

$('.button-end').on('click', (e)=>{
    ctrl.changeMarker('end');
});

$('.button-obstacle').on('click', (e)=>{
    ctrl.changeMarker('obstacle');
});

//start
$('.button-find').on('click', (e)=>{
    ctrl.findPath();
});

$('.button-clear').on('click', (e)=>{
    ctrl.clear();
});
